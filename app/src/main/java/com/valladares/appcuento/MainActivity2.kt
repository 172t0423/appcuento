package com.valladares.appcuento

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val objetoIntent: Intent =intent

        var nombre = objetoIntent.getStringExtra("nombre")
        textview_nombre.text = "$nombre"
        var edad = objetoIntent.getStringExtra("edad")
        textview_edad.text = "$edad"
        var genero = objetoIntent.getStringExtra("genero")
        textview_genero.text = "$genero"

        cuento1.setOnClickListener{
            val intent: Intent = Intent(this, MainActivity3::class.java)
            intent.putExtra("genero", genero)
            intent.putExtra("nombre", nombre)
            intent.putExtra("edad", edad)
            startActivity(intent)
        }
        cuento2.setOnClickListener{
            val intent: Intent = Intent(this, MainActivity7::class.java)
            intent.putExtra("genero", genero)
            intent.putExtra("nombre", nombre)
            intent.putExtra("edad", edad)
            startActivity(intent)
        }


        volver.setOnClickListener{
            val intent: Intent = Intent(this, MainActivity::class.java)
            intent.putExtra("genero", genero)
            intent.putExtra("nombre", nombre)
            intent.putExtra("edad", edad)
            startActivity(intent)
        }


    }
}
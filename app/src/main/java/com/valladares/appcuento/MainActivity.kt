package com.valladares.appcuento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        boton.setOnClickListener{
            if(masculino.isChecked == true) {
                val intent: Intent = Intent(this, MainActivity2::class.java)
                var genero: String = masculino.text.toString()
                intent.putExtra("genero", genero)
                var nombre: String = et_nombre.text.toString()
                intent.putExtra("nombre", nombre)
                var edad: String = et_edad.text.toString()
                intent.putExtra("edad", edad)
                if (nombre.equals("") ||  edad.equals("")) {
                    Toast.makeText(this, "Ingrese los datos faltantes ", Toast.LENGTH_SHORT).show()
                } else {
                    startActivity(intent)
                }
            }

            else{
                val intent: Intent = Intent(this, MainActivity2::class.java)
                var genero: String = femenino.text.toString()
                intent.putExtra("genero", genero)
                var nombre: String = et_nombre.text.toString()
                intent.putExtra("nombre", nombre)
                var edad: String = et_edad.text.toString()
                intent.putExtra("edad", edad)
                if (nombre.equals("") || edad.equals("")) {
                    Toast.makeText(this, "Ingrese los datos faltantes ", Toast.LENGTH_SHORT).show()
                } else {
                    startActivity(intent)
                }
            }
        }

    }
}
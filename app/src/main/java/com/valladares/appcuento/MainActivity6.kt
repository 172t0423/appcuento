package com.valladares.appcuento

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main5.*

class MainActivity6 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main6)

        val objetoIntent: Intent = intent

        var nombre = objetoIntent.getStringExtra("nombre")
        var edad = objetoIntent.getStringExtra("edad")
        var genero = objetoIntent.getStringExtra("genero")

        volver.setOnClickListener {
            val intent: Intent = Intent(this, MainActivity5::class.java)
            intent.putExtra("genero", genero)
            intent.putExtra("nombre", nombre)
            intent.putExtra("edad", edad)
            startActivity(intent)
        }
        siguiente.setOnClickListener {
            val intent: Intent = Intent(this, MainActivity2::class.java)
            intent.putExtra("genero", genero)
            intent.putExtra("nombre", nombre)
            intent.putExtra("edad", edad)
            startActivity(intent)
        }

    }
}
